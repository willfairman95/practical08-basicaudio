//
//  Counter.cpp
//  
//
//  Created by Will Fairman on 13/11/2015.
//
//

#include "Counter.h"

Counter::Counter() : Thread ("CounterThread")
{
    startThread();
}

Counter::~Counter()
{
    stopThread(500);
    
}
void Counter::stopThisThread(int time)
{
    stopThread(time);
}
void Counter::run()
{
    UInt32 counter = 0;
    
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        std::cout << "Counter:" << counter << "\n";
        Time::waitForMillisecondCounter(time + 100);
        counter += 100;
    }
}
