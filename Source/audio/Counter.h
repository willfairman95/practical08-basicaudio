//
//  Counter.h
//  
//
//  Created by Will Fairman on 13/11/2015.
//
//

#ifndef ____Counter__
#define ____Counter__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class Counter : public Thread
{
public:
    
    Counter();
    ~Counter();
    void stopThisThread(int time);
    void run() override;
    
private:
};

#endif /* defined(____Counter__) */

