//
//  SquareOscillator.cpp
//  
//
//  Created by Will Fairman on 13/11/2015.
//
//

#include "SquareOscillator.h"

#include <cmath>


float SquareOscillator::renderWaveShape (const float currentPhase)
{
    if (currentPhase < M_PI) {
        return 1.0;
    }
    else
        return 0.0;
}