//
//  Oscillator.h
//  
//
//  Created by Will Fairman on 13/11/2015.
//
//

#ifndef ____Oscillator__
#define ____Oscillator__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 Class for a sinewave oscillator
 */

class Oscillator
{
public:
    //==============================================================================
    /**
     Oscillator constructor
     */
    Oscillator();
    
    /**
     Oscillator destructor
     */
    virtual ~Oscillator();
    
    /**
     sets the frequency of the oscillator
     */
    void setFrequency (float freq);
    
    /**
     sets frequency using a midi note number
     */
    void setNote (int noteNum);
    
    /**
     sets the amplitude of the oscillator
     */
    void setAmplitude (float amp);
    
    /**
     resets the oscillator
     */
    void reset();
    
    /**
     sets the sample rate
     */
    void setSampleRate (float sr);
    
    /**
     Returns the next sample
     */
    float nextSample();
    
    /**
     function that provides the execution of the waveshape
     */
    virtual float renderWaveShape (const float currentPhase) = 0;
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
};

#endif /* defined(____Oscillator__) */
