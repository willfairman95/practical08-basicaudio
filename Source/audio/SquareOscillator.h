//
//  SquareOscillator.h
//  
//
//  Created by Will Fairman on 13/11/2015.
//
//

#ifndef ____SquareOscillator__
#define ____SquareOscillator__

#include <stdio.h>
#include "Oscillator.h"

/**
 Class for a squarewave oscillator
 */

class SquareOscillator : public Oscillator
{
public:
    //==============================================================================
    /**
     function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase) override;

};
#endif /* defined(____SquareOscillator__) */
